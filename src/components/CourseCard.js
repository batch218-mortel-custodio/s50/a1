import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

export default function CourseCard() {
  return (
    <Card style={{ width: '100%' }}>

      <Card.Body>
        <Card.Title>Sample Course</Card.Title>
        <Card.Subtitle className="mb-2">Description:</Card.Subtitle>
        <Card.Text>
          This is a sample course offering.
        </Card.Text>
        <Card.Subtitle className="mb-2">Price:</Card.Subtitle>
        <Card.Text>
          Php 40,000
        </Card.Text>
        <Button variant="primary">Enroll</Button>
      </Card.Body>
    </Card>
  );
}

