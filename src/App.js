import './App.css';

import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home';

import {Container} from 'react-bootstrap';


function App() {
  return (
    <>
    <AppNavbar />
    <Container>
      <Home />
    </Container>
    </>
  );
}

export default App;
